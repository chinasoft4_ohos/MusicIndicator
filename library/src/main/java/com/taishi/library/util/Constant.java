/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.taishi.library.util;

/**
 * 共用静态变量
 *
 * @author name
 * @since 2021-04-02
 */
public class Constant {
    /**
     * 柱条个数
     */
    public static final String INDICATOR_BAR_NUM = "bar_num";
    /**
     * 延迟
     */
    public static final String INDICATOR_DURATION = "duration";
    /**
     * 颜色
     */
    public static final String INDICATOR_BAR_COLOR = "bar_color";
    /**
     * 跳动频率
     */
    public static final String INDICATOR_STEP_NUM = "step_num";

    private Constant() {
    }
}
