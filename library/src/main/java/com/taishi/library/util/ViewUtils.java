/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.taishi.library.util;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.Optional;

/**
 * 宽高计算与换算
 *
 * @author name
 * @since 2021-04-02
 */
public class ViewUtils {
    private static ViewUtils instance = null;

    /**
     * 构造方法
     *
     * @return ViewUtils
     */
    public static synchronized ViewUtils getInstance() {
        if (instance == null) {
            instance = new ViewUtils();
        }
        return instance;
    }

    /**
     * VP转PX
     *
     * @param context
     * @param vp
     * @return float
     */
    public float vp2Px(Context context, float vp) {
        DisplayAttributes displayAttributes = getScreenPiex(context); // 获取屏幕密度
        return vp * displayAttributes.scalDensity;
    }

    /**
     * 获取attrSet
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return String
     */
    public String getAttrStringValue(AttrSet attrSet, String key, String defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getStringValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取int值的attr
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return Integer
     */
    public int getAttrIntValue(AttrSet attrSet, String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getIntegerValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取布尔值的attr
     *
     * @param attrSet
     * @param key
     * @param isDefValue
     * @return Boolean
     */
    public boolean getAttrBoolValue(AttrSet attrSet, String key, boolean isDefValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getBoolValue();
        } else {
            return isDefValue;
        }
    }

    /**
     * 获取Float值的attr
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return Float
     */
    public float getFloatValue(AttrSet attrSet, String key, float defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getFloatValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取attr
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return Integer
     */
    public int getDimensionValue(AttrSet attrSet, String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getDimensionValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取资源的attr
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return Element
     */
    public Element getAttrElementValue(AttrSet attrSet, String key, Element defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getElement();
        } else {
            return defValue;
        }
    }

    /**
     * 获取颜色attr
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return Color
     */
    public Color getAttrColorValue(AttrSet attrSet, String key, Color defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getColorValue();
        } else {
            return defValue;
        }
    }

    /**
     * 从文件获取bitmap
     *
     * @param context
     * @param mediaId
     * @return PixelMap
     * @throws NotExistException
     * @throws WrongTypeException
     * @throws IOException
     */
    public PixelMap getPixelMapFromMedia(Context context, int mediaId) throws NotExistException,
            WrongTypeException, IOException {
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        ResourceManager manager = context.getResourceManager();
        String path = manager.getMediaPath(mediaId);
        Resource resource = manager.getRawFileEntry(path).openRawFile();
        ImageSource source = ImageSource.create(resource, options);
        return source.createPixelmap(decodingOptions);
    }

    /**
     * vp转px
     *
     * @param context
     * @param dpValue
     * @return float
     */
    protected static float dip2px(Context context, float dpValue) {
        DisplayAttributes displayAttributes = getScreenPiex(context);
        return dpValue * displayAttributes.scalDensity;
    }

    /**
     * 获取屏幕宽高
     *
     * @param context
     * @return DisplayAttributes
     */
    public static DisplayAttributes getScreenPiex(Context context) {
        // 获取屏幕密度
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        return displayAttributes;
    }
}
