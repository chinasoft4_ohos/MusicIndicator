/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.taishi.library.util;

import ohos.agp.components.Component;
import ohos.agp.render.Paint;

/**
 * 测量
 *
 * @author name
 * @since 2021-04-02
 */
public class MiscUtil {
    private static final int NORMALNUM = 2;

    private MiscUtil() {
    }

    /**
     * 测量 View
     *
     * @param measureSpec
     * @param defaultSize View 的默认大小
     * @return int
     */
    public static int measure(int measureSpec, int defaultSize) {
        int result = defaultSize;
        int specMode = Component.MeasureSpec.getMode(measureSpec);
        int specSize = Component.MeasureSpec.getSize(measureSpec);

        if (specMode == Component.MeasureSpec.PRECISE) {
            result = specSize;
        } else if (specMode == Component.MeasureSpec.NOT_EXCEED) {
            result = Math.min(result, specSize);
        }
        return result;
    }

    /**
     * 获取数值精度格式化字符串
     *
     * @param precision
     * @return String
     */
    public static String getPrecisionFormat(int precision) {
        return "%." + precision + "f";
    }

    /**
     * 反转数组
     *
     * @param arrays
     * @param <T>
     * @return T
     */
    public static <T> T[] reverse(T[] arrays) {
        if (arrays == null) {
            return arrays;
        }
        int length = arrays.length;
        for (int ii = 0; ii < length / NORMALNUM; ii++) {
            T tt = arrays[ii];
            arrays[ii] = arrays[length - ii - 1];
            arrays[length - ii - 1] = tt;
        }
        return arrays;
    }

    /**
     * 测量文字高度
     *
     * @param paint
     * @return float
     */
    public static float measureTextHeight(Paint paint) {
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        return Math.abs(fontMetrics.ascent) - fontMetrics.descent;
    }
}