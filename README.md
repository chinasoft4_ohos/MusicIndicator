﻿# MusicIndicator
## 项目介绍

 * 项目名称：MusicIndicator
 * 所属系列：openharmony的第三方组件适配移植
 * 功能：显示快慢交替的柱状条，可用于配合音乐
 * 项目移植状态：已完成
 * 调用差异：无
 * 开发版本：sdk6，DevEco Studio 2.2 Beta1
 * 基线版本：master分支

## 效果演示
<img src="gif/screen.gif"></image>

## 安装教程
在moudle级别下的build.gradle文件中添加依赖
```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
```
在entry模块中的build.gradle文件中添加
```
dependencies {
    implementation ('com.gitee.chinasoft_ohos:MusicIndicator:1.0.0')
    ····
}
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行，

如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，

并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
柱状条使用此组件
```
    <com.taishi.library.Indicator
        ohos:height="200vp"
        ohos:width="match_parent"
        ohos:background_element="#000000"
        app:bar_color="#1DD069"
        app:bar_num="40"
        app:duration="20000"
        app:step_num="50"
        />
```

## 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
> * 1.0.0


## 版权和许可信息

```
Copyright 2017 Taishi Yamasaki

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```